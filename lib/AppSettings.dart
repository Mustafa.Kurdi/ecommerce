import 'package:ecommerce/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:r_upgrade/r_upgrade.dart';
import 'package:easy_localization/easy_localization.dart';
import 'core/constants.dart';


class AppSettings {

  static const String BASE_URL = 'https://api.orianosy.com/shop/test';
  static const String EAV_BASE_URL = 'http://213.178.225.146:83';
  static const String APK_URL = 'http://greentreasure.co/stemcells.apk';
  static const String VERSION = '1.0.0:1';


  static showUpdatePopup(){
    showCupertinoModalPopup<void>(
      context: MyApp.navigatorKey.currentContext,
      builder: (BuildContext context) => CupertinoActionSheet(

        title:   Text('Settings'.tr()),
        message:  Text(APP_NAME+ ' $VERSION'),
        actions: <CupertinoActionSheetAction>[
          CupertinoActionSheetAction(
            child:  Text('Update Application'.tr()),
            onPressed: () {
              Navigator.pop(context);
              upgradeFromUrl();
            },
          ),

        ],
        cancelButton: CupertinoActionSheetAction(
          child:  Text("Cancel".tr() ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }

  static void upgradeFromUrl() async {

    int id = await RUpgrade.upgrade(APK_URL,
        fileName: 'app.apk', isAutoRequestInstall: true);

    final snackBar = SnackBar(
        action: SnackBarAction(
          label: 'Stop' ,
          textColor: Colors.white,
          onPressed: () {
            RUpgrade.cancel(id);
          },
        ),
        duration: Duration(seconds: 100),

        content: StreamBuilder(
          stream: RUpgrade.stream,
          builder: (context, snapshot) {
            if(snapshot.hasData)
            {
              var d = snapshot.data as DownloadInfo;
              return Text(d.percent.toString() + '%');
            }
            else return Text('');
          },
        ));


    ScaffoldMessenger.of(MyApp.navigatorKey.currentContext).showSnackBar(snackBar);

  }

}
