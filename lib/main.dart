import 'package:easy_localization/easy_localization.dart';
import 'package:ecommerce/core/constants.dart';
import 'package:ecommerce/features/splash/presentation/pages/Splash.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer_util.dart';
import 'core/constants/AppColors.dart';
import 'core/utils/SharedPreferences/SharedPreferencesHelper.dart';
import 'core/constants/AppTheme.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppSharedPreferences.init();

  Locale startLocale;
  bool isSaveLang = AppSharedPreferences.hasAccessLang;
  if(isSaveLang) {
    String lang = AppSharedPreferences.lang;
    if (lang == null) {
      AppSharedPreferences.lang = DEFAULT_LANGUAGE;
      startLocale = LANGUAGES[DEFAULT_LANGUAGE];
    }
    else
      startLocale = LANGUAGES[lang];
  }else{
    startLocale = LANGUAGES["ar"];
  }


  SystemChrome.setPreferredOrientations(
    [
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ],
  ).then((val) {
  runApp(
    EasyLocalization(
      startLocale:startLocale,
      supportedLocales: LANGUAGES.values.toList(),
      path: "assets/lang",
      child: Sizer()
    ),
  );
  });
}


class Sizer extends StatelessWidget {
  final Widget child;

  const Sizer({Key key, this.child}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizerUtil().init(constraints, orientation);
            return MyApp();
          },
        );
      },
    );
  }
}
class MyApp extends StatelessWidget {
  static final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      navigatorKey: navigatorKey,
      title: APP_NAME.tr(),
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: AppColors.Primary,
        accentColor: AppColors.Primary,
        textTheme: AppTheme.textTheme,
        primaryTextTheme: AppTheme.textTheme,
        iconTheme: IconThemeData(color: AppColors.Primary),
        primaryIconTheme: IconThemeData(color: Colors.white),

        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Splash(),
      debugShowCheckedModeBanner: false,

      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
    );
  }
}
