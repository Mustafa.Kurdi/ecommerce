import 'dart:async';
import 'dart:io';
import 'package:ecommerce/core/constants/AppAssets.dart';
import 'package:ecommerce/core/constants/AppColors.dart';
import 'package:ecommerce/core/constants/AppTheme.dart';
import 'package:ecommerce/core/utils/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:ecommerce/features/Intro/presentation/pages/Intro.dart';
import 'package:ecommerce/features/stores/presentation/pages/Store.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:connectivity/connectivity.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    checkNet();
  }


  checkNet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          title: Text("Unable to connect to the Internet".tr(),style: AppTheme.headline2,),
          actions: <Widget>[
            FlatButton(
              onPressed: () => {
                Navigator.pop(context),
                checkNet(),
              },
              child: Text("try".tr(),style: TextStyle(color: AppColors.Primary),),
            ),
            FlatButton(
              onPressed: () => exit(0),
              child: Text("Close".tr(),style: TextStyle(color: AppColors.UI_Red),),
            ),
          ],
        ),
      );
    }
    else{
      startTimer();
    }
  }
  void startTimer() {
    Timer(Duration(seconds: 3), () {
      navigateUser(); //It will redirect  after 3 seconds
    });
  }

  void navigateUser() async {
    if (AppSharedPreferences.hasAccessIntro) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Store()));
    }else {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => PageIntro()));
    }
  }

  // void navigateUser() async {
  //   if (AppSharedPreferences.accessFirstTime) {
  //     if (AppSharedPreferences.hasAccessToken) {
  //       Navigator.pushReplacement(
  //           context, MaterialPageRoute(builder: (context) => DrawerPage()));
  //     } else {
  //       Navigator.pushReplacement(
  //           context, MaterialPageRoute(builder: (context) => Login()));
  //     }
  //   } else {
  //     Navigator.pushReplacement(
  //         context, MaterialPageRoute(builder: (context) => PageIntro()));
  //   }
  // }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.UI_White,
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 10.0.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.0.w),
              child: Container(
                height: 25.0.h,
                width: 50.0.w,
                decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage(AppAssets.Logo), fit: BoxFit.fill)),
              ),
            ),
            SizedBox(
              height: 10.0.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.0.w),
              child: SizedBox(
                width: 50.0.w,
                height: 15.0.h,
                child: Center(
                  child: DefaultTextStyle(
                    style: const TextStyle(
                      fontSize: 35,
                      color: Colors.white,
                      shadows: [
                        Shadow(
                          blurRadius: 7.0,
                          color: Colors.white,
                          offset: Offset(0, 0),
                        ),
                      ],
                    ),
                    child: AnimatedTextKit(
                      repeatForever: true,
                      isRepeatingAnimation: true,
                      totalRepeatCount: 2,
                      animatedTexts: [
                        FlickerAnimatedText("App Name".tr()),
                      ],
                      onTap: () {
                        print("Tap Event");
                      },
                    ),
                  ),
                ),
              ),
            ),
          DefaultTextStyle(
            style: const TextStyle(
              fontSize: 20.0,
            ),
            child: AnimatedTextKit(
              animatedTexts: [
                WavyAnimatedText("1.0.0"),
              ],
              isRepeatingAnimation: false,
              onTap: () {
                print("Tap Event");
              },
            ),
          ),
            SizedBox(
              height: 25.0.h,
            ),
            Container(
              height: 0.8.h,
              width: 50.0.w,
              child: LinearProgressIndicator(
                backgroundColor: AppColors.Primary,
                minHeight: 5,
                valueColor: AlwaysStoppedAnimation<Color>(AppColors.Gray_Style_1),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
