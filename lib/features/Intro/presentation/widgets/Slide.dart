import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Slide extends StatelessWidget {
  final String text;
  final Widget image;
  const Slide({Key key, this.text, this.image}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
                child: Container()
            ),
            Expanded(
              child: Text(text,
                  textAlign: TextAlign.center,
              ),
            ),
            Expanded(
              flex: 10,
              child: image,
            ),
          ],
        ),
      ),
    );
  }
}