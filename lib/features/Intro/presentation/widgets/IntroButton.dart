import 'package:ecommerce/core/constants/AppColors.dart';
import 'package:flutter/material.dart';

class IntroButton extends StatelessWidget {
  final String text;

  final Function onClick;

  const IntroButton({@required this.text, @required this.onClick});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      splashColor: AppColors.Primary,
      child: Text(
        "$text",
      ),
      onPressed: onClick,
    );
  }
}
