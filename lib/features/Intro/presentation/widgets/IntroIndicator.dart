import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class IntroIndicator extends StatelessWidget {

  final int pageNumber;
  final int index;
  final Color activeColor;
  final Color inActiveColor;

  final bool isCircle;
  const IntroIndicator({this.index, this.pageNumber,@required this.activeColor,@required this.inActiveColor, this.isCircle});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: List.generate(pageNumber, (i) {
          return _dot( index == i ? true : false);
        }),
      ),
    );
  }
  _dot(bool active){
     const _kDuration = const Duration(milliseconds: 700);
     const _kCurves =  Curves.linear;
    return Padding(
      padding: EdgeInsets.all(1.5.w),
      child: isCircle ? AnimatedContainer(
        curve: _kCurves,
        duration: _kDuration,
        height:active ? 20.0.sp : 15.0.sp,
        width: active ? 20.0.sp : 15.0.sp,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
          color: active ? activeColor: inActiveColor,
        ),
      ) : AnimatedContainer(
        curve: _kCurves,
        duration: _kDuration,
        height: 1.0.h ,
        width: active ? 9.7.w : 5.5.w,
        color: active ? activeColor: inActiveColor,
      ),
    );
  }
}
