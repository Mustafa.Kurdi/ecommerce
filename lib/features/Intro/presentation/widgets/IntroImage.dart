import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';

class IntroImage extends StatelessWidget{

  final String url_image;

  const IntroImage({@required this.url_image});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5.0.w),
      child: Container(
        width: 90.0.w,
        height: 40.0.h,
        child: SvgPicture.asset(url_image),
        // child: GifImage(controller: _controllerGif,image: AssetImage(widget.url_image),),
      ),
    );
  }


}
