import 'package:ecommerce/core/constants/AppAssets.dart';
import 'package:ecommerce/core/constants/AppColors.dart';
import 'package:ecommerce/core/utils/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:ecommerce/features/Intro/presentation/widgets/IntroButton.dart';
import 'package:ecommerce/features/Intro/presentation/widgets/IntroIndicator.dart';
import 'package:ecommerce/features/stores/presentation/pages/Store.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

import '../widgets/Slide.dart';

class PageIntro extends StatelessWidget {

  List<Widget> _pages = [
    Slide(
      text: 'Text Anonymous 1',
      image: Image.asset(AppAssets.Logo),
    ),
    Slide(
      text: 'Text Anonymous 2',
      image: Image.asset(AppAssets.Logo),
    )

  ];

  @override
  Widget build(BuildContext context) {
    return Intro(
      pages: _pages,
      activeColor: AppColors.Primary,
      inActiveColor: AppColors.Secondary,
      next: "next".tr(),
      ok: "ok".tr(),
      previous: "previous".tr(),
      skip: "skip".tr(),
      isCircle: true,
    );
  }
}

class Intro extends StatefulWidget {
  final List<Widget> pages;
  final String ok;
  final String previous;
  final String next;
  final String skip;

  final Color activeColor;
  final Color inActiveColor;

  final bool isCircle;

  const Intro({Key key,
    this.pages,
    this.ok ='ok',
    this.previous='previous',
    this.next='next',
    this.skip='skip',
    this.activeColor,
    this.inActiveColor, this.isCircle
  }) : super(key: key);
  @override
  _IntroState createState() => _IntroState();
}

class _IntroState extends State<Intro> {
  var _pages ;
  int _seletedItem = 0;



  PageController _controllerPageView ;

  static const _kDuration = const Duration(milliseconds: 300);
  static const _kCurve = Curves.ease;

  @override
  void initState() {
    _pages = widget.pages;
    _pages.add(Container());
    _controllerPageView = PageController();
    super.initState();
  }

  @override
  void dispose() {
    _controllerPageView.dispose();
    super.dispose();
  }


  // Future<void> permissionHandler()async{
  //   var statusLocation = await Permission.location.status;
  //   if (statusLocation.isDenied) {
  //     await Permission.location.request();
  //     if (statusLocation  == PermissionStatus.granted) {
  //       print('Permission granted');
  //     } else if (statusLocation == PermissionStatus.denied) {
  //       print('Permission denied. Show a dialog and again ask for the permission');
  //     } else if (statusLocation == PermissionStatus.permanentlyDenied) {
  //       print('Take the user to the settings page.');
  //       await openAppSettings();
  //     }
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.UI_White,
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Expanded(
              flex: 9,
              child: PageView(
                children: _pages,
                scrollDirection: Axis.horizontal,
                controller: _controllerPageView,
                onPageChanged: (index) {
                  print(index);
                  print(_pages.length);
                  setState(() {
                    _seletedItem = index;

                    if(index > _pages.length - 2)
                    {
                      // permissionHandler();
                      AppSharedPreferences.intro = true;
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (context) => Store(),),);
                    }
                  });
                },
              ),
            ),
            Expanded(
              child: IntroIndicator(
                isCircle: widget.isCircle,
                index: _seletedItem,
                pageNumber: _pages.length - 1,
                activeColor: widget.activeColor ?? AppColors.Primary,
                inActiveColor: widget.inActiveColor ?? AppColors.Secondary,
              ),
            ),

          ],
        ),
      ),
        bottomNavigationBar : Padding(
          padding:  EdgeInsets.only(bottom: 2.0.h),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IntroButton(
                text:  _seletedItem ==0 ? widget.skip : widget.previous,
                onClick: () {
                  if(_seletedItem ==0)
                  {
                    // permissionHandler();
                    AppSharedPreferences.intro = true;
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => Store(),),);
                  }
                  else{
                    _controllerPageView.previousPage(duration: _kDuration, curve: _kCurve);
                  }
                },
              ),
              IntroButton(
                text: _seletedItem == 2 ? widget.ok : widget.next,
                onClick: () {
                  if(_seletedItem ==_pages.length - 2){
                    // permissionHandler();
                    AppSharedPreferences.intro = true;
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => Store(),),);
                  }
                  else{
                    _controllerPageView.nextPage(duration: _kDuration, curve: _kCurve);
                  }
                },
              ),
            ],
          ),
        ),
    );
  }
}
