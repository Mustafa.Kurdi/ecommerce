import 'package:ecommerce/core/coreModels/base_result_model.dart';

class StoresResponseModel extends BaseResultModel{
  List<Store> stores;

  StoresResponseModel({this.stores});

  StoresResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['result'] != null) {
      stores = new List<Store>();
      json['result'].forEach((v) {
        stores.add(new Store.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.stores != null) {
      data['result'] = this.stores.map((v) => v.toJson()).toList();
    }
    return data;
  }

  @override
  List<Object> get props => [];
}

class Store {
  ShopName shopName;
  ShopName description;
  MinimumOrder minimumOrder;
  Address address;
  List<String> contactInfo;
  List<String> deliveryRegions;
  List<dynamic> paymentMethod;
  String sId;
  String ownerFullName;
  String profilePhoto;
  String coverPhoto;
  String menu;
  String operation;
  String review;
  String createdAt;
  String updatedAt;
  int iV;
  String categoryType;
  String estimatedDeliveryTime;
  String deliveryFeeType;
  dynamic deliveryFeeTag;
  bool enable;
  String badgeTag;
  bool availability;

  Store(
      {this.shopName,
        this.description,
        this.minimumOrder,
        this.address,
        this.contactInfo,
        this.deliveryRegions,
        this.paymentMethod,
        this.sId,
        this.ownerFullName,
        this.profilePhoto,
        this.coverPhoto,
        this.menu,
        this.operation,
        this.review,
        this.createdAt,
        this.updatedAt,
        this.iV,
        this.categoryType,
        this.estimatedDeliveryTime,
        this.deliveryFeeType,
        this.deliveryFeeTag,
        this.enable,
        this.badgeTag,
        this.availability});

  Store.fromJson(Map<String, dynamic> json) {
    shopName = json['shopName'] != null
        ? new ShopName.fromJson(json['shopName'])
        : null;
    description = json['description'] != null
        ? new ShopName.fromJson(json['description'])
        : null;
    minimumOrder = json['minimumOrder'] != null
        ? new MinimumOrder.fromJson(json['minimumOrder'])
        : null;
    address =
    json['address'] != null ? new Address.fromJson(json['address']) : null;
    contactInfo = json['contactInfo'].cast<String>();
    deliveryRegions = json['deliveryRegions'].cast<String>();
    paymentMethod  =json['paymentMethod'];
    sId = json['_id'];
    ownerFullName = json['ownerFullName'];
    profilePhoto = json['profilePhoto'];
    coverPhoto = json['coverPhoto'];
    menu = json['menu'];
    operation = json['operation'];
    review = json['review'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
    categoryType = json['categoryType'];
    estimatedDeliveryTime = json['estimatedDeliveryTime'];
    deliveryFeeType = json['deliveryFeeType'];
    deliveryFeeTag = json['deliveryFeeTag'];
    enable = json['enable'];
    badgeTag = json['badgeTag'];
    availability = json['availability'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.shopName != null) {
      data['shopName'] = this.shopName.toJson();
    }
    if (this.description != null) {
      data['description'] = this.description.toJson();
    }
    if (this.minimumOrder != null) {
      data['minimumOrder'] = this.minimumOrder.toJson();
    }
    if (this.address != null) {
      data['address'] = this.address.toJson();
    }
    data['contactInfo'] = this.contactInfo;
    data['deliveryRegions'] = this.deliveryRegions;
    data['paymentMethod'] = this.paymentMethod;
    data['_id'] = this.sId;
    data['ownerFullName'] = this.ownerFullName;
    data['profilePhoto'] = this.profilePhoto;
    data['coverPhoto'] = this.coverPhoto;
    data['menu'] = this.menu;
    data['operation'] = this.operation;
    data['review'] = this.review;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    data['categoryType'] = this.categoryType;
    data['estimatedDeliveryTime'] = this.estimatedDeliveryTime;
    data['deliveryFeeType'] = this.deliveryFeeType;
    data['deliveryFeeTag'] = this.deliveryFeeTag;
    data['enable'] = this.enable;
    data['badgeTag'] = this.badgeTag;
    data['availability'] = this.availability;
    return data;
  }
}

class ShopName {
  String en;
  String ar;

  ShopName({this.en, this.ar});

  ShopName.fromJson(Map<String, dynamic> json) {
    en = json['en'];
    ar = json['ar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['en'] = this.en;
    data['ar'] = this.ar;
    return data;
  }
}

class MinimumOrder {
  int amount;
  String currency;

  MinimumOrder({this.amount, this.currency});

  MinimumOrder.fromJson(Map<String, dynamic> json) {
    amount = json['amount'];
    currency = json['currency'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amount'] = this.amount;
    data['currency'] = this.currency;
    return data;
  }
}

class Address {
  String city;
  String country;
  String otherDetails;
  String state;
  String street;

  Address(
      {this.city, this.country, this.otherDetails, this.state, this.street});

  Address.fromJson(Map<String, dynamic> json) {
    city = json['city'];
    country = json['country'];
    otherDetails = json['otherDetails'];
    state = json['state'];
    street = json['street'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['city'] = this.city;
    data['country'] = this.country;
    data['otherDetails'] = this.otherDetails;
    data['state'] = this.state;
    data['street'] = this.street;
    return data;
  }
}