part of 'store_cubit.dart';

@immutable
abstract class StoreState {}

class StoreInitial extends StoreState {}

class StoreLoading extends StoreState {}

class StoreSuccessfully extends StoreState {
  final List<Store> stores;

  StoreSuccessfully({this.stores});
}

class StoreError extends StoreState {
  final String message;

  StoreError({this.message});

}