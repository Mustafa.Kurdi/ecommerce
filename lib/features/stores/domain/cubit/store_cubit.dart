import 'package:bloc/bloc.dart';
import 'package:ecommerce/core/api/Network.dart';
import 'package:ecommerce/core/errors/base_error.dart';
import 'package:ecommerce/features/stores/data/StoresResponseModel.dart';
import 'package:meta/meta.dart';
part 'store_state.dart';

class StoreCubit extends Cubit<StoreState> {
  StoreCubit() : super(StoreInitial());

  Future<void> getAllStore() async {
      emit(StoreLoading());
    final result = await Network.stores();
    if (result is StoresResponseModel) {
        emit(StoreSuccessfully(stores: result.stores));
    } else if (result is BaseError) {
      emit(StoreError(message: result.props.first));
    } else {
      emit(StoreError(message: "Error"));
    }
  }

}
