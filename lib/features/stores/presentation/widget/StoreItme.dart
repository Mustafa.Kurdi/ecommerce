import 'package:cached_network_image/cached_network_image.dart';
import 'package:ecommerce/core/constants/AppColors.dart';
import 'package:ecommerce/core/constants/AppTheme.dart';
import 'package:ecommerce/core/utils/Converters/Converters.dart';
import 'package:ecommerce/features/stores/data/StoresResponseModel.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:sizer/sizer.dart';

class StoreItme extends StatelessWidget {
  final bool isLang;
  final Store store;

  const StoreItme({this.isLang, this.store});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.0.w,
      padding: EdgeInsets.all(15.0.sp),
      child: Column(
        children: [
          Container(
            width: 100.0.w,
            height: 20.0.h,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(18.0.sp),
              ),
            ),
            child: CachedNetworkImage(
              imageUrl: store.coverPhoto,
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(18.0.sp),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: AppColors.Primary.withOpacity(0.5),
                      blurRadius: 7,
                      offset: Offset(0, 6), // changes position of shadow
                    ),
                  ],
                  image: DecorationImage(
                      image: imageProvider,
                      fit: BoxFit.cover,
                      ),
                ),
              ),
              placeholder: (context, url) => Center(child: CircularProgressIndicator()),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
          Container(
            width: 100.0.w,
            padding: EdgeInsets.symmetric(horizontal: 3.0.sp, vertical: 1.0.h),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Text(
                          isLang ? store.shopName.ar : store.shopName.en,
                          style: AppTheme.headline6.copyWith(color: AppColors.Primary),
                        ),
                        Icon(
                          Icons.circle,
                          color: store.availability
                              ? AppColors.UI_Green
                              : AppColors.UI_Red,
                          size: 15.0.sp,
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Row(
                          children: [
                            Icon(Icons.timer_rounded),
                            Text(
                                  " "+store.estimatedDeliveryTime.substring(0, 3) +
                                  MyConverter.cut3Char(store.estimatedDeliveryTime)
                                      .tr(),
                              style: AppTheme.bodyText1.copyWith(fontSize: 10.0.sp),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Icon(Icons.arrow_drop_down),
                            Text(
                                  " "+store.minimumOrder.currency+" "+store.minimumOrder.amount.toString(),
                              style: AppTheme.bodyText1.copyWith(fontSize: 10.0.sp),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                Text(
                  isLang ? store.description.ar : store.description.en,
                  style: AppTheme.headline1,
                ),
                Container(
                  padding: EdgeInsets.only(top: 1.0.h),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.location_on),
                      SizedBox(
                          width: 80.0.w,
                          child: Text(store.address.street+" "+store.address.otherDetails,maxLines: 3,)),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
