import 'dart:async';
import 'dart:io';
import 'package:ecommerce/core/constants.dart';
import 'package:ecommerce/core/constants/AppColors.dart';
import 'package:ecommerce/core/constants/AppTheme.dart';
import 'package:ecommerce/core/utils/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:ecommerce/core/widgets/CustomSmallBottom.dart';
import 'package:ecommerce/core/widgets/snackbar.dart';
import 'package:ecommerce/features/splash/presentation/pages/Splash.dart';
import 'package:ecommerce/features/stores/domain/cubit/store_cubit.dart';
import 'package:ecommerce/features/stores/presentation/widget/StoreItme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

class Store extends StatefulWidget {
  @override
  _StoreState createState() => _StoreState();
}

class _StoreState extends State<Store> {
  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  StoreCubit cubit = StoreCubit();

  final GlobalKey<PopupMenuButtonState<int>> _key = GlobalKey();
  RefreshController _refresherController = RefreshController();

  Future<bool> _onWillPop() {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            title: Text('Are you sure?'.tr()),
            content: Text('Do you want to exit an App'.tr()),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text(
                  'no'.tr(),
                  style: AppTheme.headline3.copyWith(color: AppColors.Primary),
                ),
              ),
              FlatButton(
                onPressed: () => exit(0),
                /*Navigator.of(context).pop(true)*/
                child: Text(
                  "yes".tr(),
                  style: AppTheme.headline3.copyWith(color: AppColors.UI_Red),
                ),
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        backgroundColor: AppColors.Primary,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: AppColors.Primary,
          title: Text(
            "store".tr(),
            style: AppTheme.headline6.copyWith(color: AppColors.UI_White),
          ),
          actions: [
            InkWell(
              onTap: () {
                _key.currentState.showButtonMenu();
              },
              child: PopupMenuButton<int>(
                key: _key,
                onSelected: (i) {
                  if (i == 0)
                    showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (BuildContext context) {
                        // return object of type Dialog
                        return ChangeLanguage(
                            lang: (context.locale == Locale('ar'))
                                ? true
                                : false);
                      },
                    );
                },
                itemBuilder: (context) {
                  return <PopupMenuEntry<int>>[
                    PopupMenuItem(
                        child: Text("change language".tr()), value: 0),
                  ];
                },
              ),
            ),
          ],
          elevation: 0,
        ),
        body: Container(
          width: 100.0.w,
          height: 100.0.h,
          decoration: BoxDecoration(
            color: AppColors.UI_White,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(40.0.sp),
              topLeft: Radius.circular(40.0.sp),
            ),
          ),
          clipBehavior: Clip.antiAliasWithSaveLayer,
          child: BlocProvider(
            create: (context) => cubit..getAllStore(),
            child: BlocConsumer<StoreCubit, StoreState>(
              builder: (context, state) {
                if (state is StoreLoading)
                  return Center(
                    child: CircularProgressIndicator(
                      backgroundColor: AppColors.Primary,
                      valueColor:
                          AlwaysStoppedAnimation<Color>(AppColors.Gray_Style_1),
                    ),
                  );
                else if (state is StoreSuccessfully) {
                  return SmartRefresher(
                    onLoading: (){},
                    onRefresh: (){
                      cubit..getAllStore();
                    },
                    controller: _refresherController,
                    child: ListView.separated(
                      separatorBuilder: (context, index) {
                        return Divider(
                          color: AppColors.Gray_Style_1,
                          height: 1,
                        );
                      },
                      itemBuilder: (context, index) {
                        return AnimationConfiguration.staggeredList(
                          position: index,
                          duration: const Duration(milliseconds: 375),
                          child: SlideAnimation(
                            verticalOffset: 50.0,
                            child: FadeInAnimation(
                              child: StoreItme(
                                isLang:
                                    context.locale == Locale("ar") ? true : false,
                                store: state.stores[index],
                              ),
                            ),
                          ),
                        );
                      },
                      itemCount: state.stores.length,
                    ),
                  );
                } else if (state is StoreError)
                  return Center(
                    child: MaterialButton(
                      onPressed: () => cubit..getAllStore(),
                      child: Text(
                        "Tap to retry".tr(),
                        style: TextStyle(
                            fontSize: 14.0.sp,
                            fontWeight: FontWeight.bold,
                            color: AppColors.Primary),
                      ),
                    ),
                  );
                else
                  return Container();
              },
              listener: (context, state) {
                if (state is StoreError) showSnackBar(state.message);
              },
            ),
          ),
        ),
      ),
    );
  }
}

class ChangeLanguage extends StatefulWidget {
  final bool lang;

  ChangeLanguage({this.lang});

  @override
  _ChangeLanguageState createState() => _ChangeLanguageState();
}

class _ChangeLanguageState extends State<ChangeLanguage> {
  SingingCharacter _character;

  @override
  void initState() {
    _character =
        widget.lang ? SingingCharacter.Arabic : SingingCharacter.English;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Text(
        "select a language , the application will restart".tr(),
      ),
      content: Container(
        padding: EdgeInsets.symmetric(horizontal: 5.0.w),
        width: 70.0.w,
        color: AppColors.UI_White,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            RadioListTile<SingingCharacter>(
              activeColor: AppColors.darkBlue,
              value: SingingCharacter.English,
              groupValue: _character,
              onChanged: (SingingCharacter value) {
                setState(() {
                  _character = value;
                });
              },
              title: Text(
                "English",
              ),
            ),
            RadioListTile<SingingCharacter>(
              activeColor: AppColors.darkBlue,
              value: SingingCharacter.Arabic,
              groupValue: _character,
              onChanged: (SingingCharacter value) {
                setState(() {
                  _character = value;
                });
              },
              title: Text(
                "العربية",
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  CustomSmallButton(
                    buttonSize: true,
                    onClick: () {
                      AppSharedPreferences.lang =
                          _character == SingingCharacter.Arabic ? "ar" : "en";
                      context.locale = Locale(
                          _character == SingingCharacter.Arabic ? "ar" : "en");
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (context) => Splash()),
                      );
                    },
                    text: "save".tr(),
                    buttonColor: AppColors.Primary,
                    textColor: AppColors.UI_White,
                  ),
                  SizedBox(
                    width: 3.0.w,
                  ),
                  CustomSmallButton(
                    buttonSize: true,
                    onClick: () {
                      Navigator.pop(context);
                    },
                    text: "cancel".tr(),
                    buttonColor: AppColors.UI_White,
                    textColor: AppColors.black,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
