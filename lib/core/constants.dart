import 'package:flutter/material.dart';

const APP_NAME = 'App Name';



// Languages
const DEFAULT_LANGUAGE = 'ar';
const Map<String,Locale> LANGUAGES = {
  'ar':Locale('ar'),
  'en': Locale('en'),
};


// Headers
const HEADER_LANGUAGE = 'Language';
const HEADER_AUTH = 'authorization';
const HEADER_CONTENT_TYPE = 'Content-Type';
const HEADER_ACCEPT = 'accept';




enum Gender {
  unspecified ,male , female
}

enum NotificationsState{
  Unread,  Read
}


enum SingingCharacter { Arabic, English }