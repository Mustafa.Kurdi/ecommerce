import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';


class AppColors {

  /*  my Colors  */
  static const Color transparent = Color(0x00000000);
  static const Color Primary = Color(0xFFf1b71c);
  static const Color Secondary = Color(0xFFFFE0A5);
  static const Color darkBlue = Color(0xFFEF5CA1);
  static const Color lightBlue = Color(0xFFA3D9E5);
  static const Color Black = Color(0xFF333333);
  static const Color black = Color(0xFF171815);
  static const Color Gray_Style_1 = Color(0xFF888888);
  static  Color Gray_800 = Colors.grey.shade800;
  static const Color Gray_style_2 = Color(0xFFCBCBCB);
  static const Color UI_White = Color(0xFFFFFFFF);
  static const Color UI_Red = Colors.redAccent;
  static const Color UI_Green = Colors.green;


  static const Color Inway = Color(0xFF9B51E0);
  static const Color Delivered = Color(0xFF219653);

  // static const Color UI_White = Color(0xFF423737);

}