class AppAssets {
  AppAssets._();

  //image for Logo
  static const Logo = 'assets/images/logo.png';

  // empty data
  static const EmptyBoxGif = 'assets/images/emptybox.gif';
  static const EmptyBoxPng = 'assets/images/EmptyBox.png';

}
