import 'package:flutter/material.dart';
import 'AppColors.dart';
import 'package:sizer/sizer.dart';

class AppTheme
{

  static  TextTheme textTheme = TextTheme(
    headline1:headline1 ,
    headline2:headline2 ,
    headline3:headline3 ,
    headline4: headline4,
    headline5: headline5,
    headline6: headline6,
    subtitle1: subtitle1,
    subtitle2: subtitle2,
    bodyText2: bodyText2,
    bodyText1: bodyText1,
    caption: caption,
    overline: overLine
  );

  static const String fontTaxicab = 'Taxicab';


  static  TextStyle overLine = TextStyle(
    fontFamily: fontTaxicab,
    fontWeight: FontWeight.bold,
    fontSize: 12.0.sp,
    letterSpacing: 0.4,
    height: 0.9,
    color: AppColors.black.withOpacity(0.5),
  );

  static  TextStyle headline1 = TextStyle(
    fontFamily: fontTaxicab,
    // fontWeight: FontWeight.bold,
    fontSize: 13.0.sp,
    // letterSpacing: 0.4,
    height: 0.9,
    color: AppColors.black,
  );

  static  TextStyle headline2 = TextStyle(
    fontFamily: fontTaxicab,
    fontWeight: FontWeight.bold,
    fontSize: 14.0.sp,
    letterSpacing: 0.4,
    height: 0.9,
    color: AppColors.black,
  );
  static  TextStyle headline3 = TextStyle(
    fontFamily: fontTaxicab,
    fontWeight: FontWeight.w400,
    fontSize: 15.0.sp,
    letterSpacing: 0.4,
    height: 0.9,
    color: AppColors.black,
  );

  static  TextStyle headline4 = TextStyle(
    fontFamily: fontTaxicab,
    fontWeight: FontWeight.bold,
    fontSize: 12.0.sp,
    letterSpacing: 0.4,
    height: 0.9,
    color: AppColors.black,
  );

  static  TextStyle headline5 = TextStyle(
    fontFamily: fontTaxicab,
    fontWeight: FontWeight.bold,
    fontSize: 12.0.sp,
    letterSpacing: 0.4,
    height: 0.9,
    color: AppColors.black,
  );

  //appbar title
  static  TextStyle headline6 = TextStyle(
    fontFamily: fontTaxicab,
    fontWeight: FontWeight.bold,
    fontSize: 18.0.sp,
    letterSpacing: 0.18,
    color: Colors.black,
  );

  static  TextStyle subtitle1 = TextStyle(
    fontFamily: fontTaxicab,
    fontWeight: FontWeight.w400,
    fontSize: 14.0.sp,
    letterSpacing: -0.04,
    color: AppColors.black,
  );

  static const TextStyle subtitle2 = TextStyle(
    fontFamily: fontTaxicab,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.04,
    color: AppColors.black,
  );

  static  TextStyle bodyText2 = TextStyle(
    fontFamily: fontTaxicab,
    fontWeight: FontWeight.w400,
    fontSize: 13.0.sp,
//    letterSpacing: 0.2,
    color: AppColors.black,
  );

  static  TextStyle bodyText1 = TextStyle(
    fontFamily: fontTaxicab,
    fontWeight: FontWeight.w400,
    fontSize: 16.0.sp,
    letterSpacing: -0.05,
    color: AppColors.black,
  );

  static const TextStyle caption = TextStyle(
    fontFamily: fontTaxicab,
    fontWeight: FontWeight.w400,
    fontSize: 12,
    letterSpacing: 0.2,
    color: AppColors.black, // was lightText
  );



}