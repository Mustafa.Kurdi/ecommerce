
import 'package:flutter/widgets.dart';

class AppIcons {

  static const _kFontFam = 'AppIcons';
  static const String _kFontPkg = null;

  static const IconData group_1 = IconData(0xe809, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData group_2 = IconData(0xe80a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData group_3 = IconData(0xe80b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData group_4 = IconData(0xe80c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData group_5 = IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData group_6 = IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData group_7 = IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData group_8 = IconData(0xe803, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData group_9 = IconData(0xe804, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData group_10 = IconData(0xe80d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData group_11 = IconData(0xe80e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData group_12 = IconData(0xe80f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData group_13 = IconData(0xe805, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData group_14 = IconData(0xe806, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData group_15 = IconData(0xe807, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData group_16 = IconData(0xe808, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData group_17 = IconData(0xe810, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData group_18 = IconData(0xe811, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
