import 'package:ecommerce/AppSettings.dart';

class ApiUrl {

  // BASIC URL
  static const String BASICURL = AppSettings.BASE_URL;



  // get all stores
  static const String GET_STORES = BASICURL + "/find/all/shop";

}
