import 'package:ecommerce/core/constants/AppColors.dart';
import 'package:ecommerce/core/constants/AppTheme.dart';
import 'package:ecommerce/main.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';


showSnackBar(String message){
  ScaffoldMessenger.of(MyApp.navigatorKey.currentContext).hideCurrentSnackBar();
  ScaffoldMessenger.of(MyApp.navigatorKey.currentContext)
      .showSnackBar(snackBar(message.toString()));
}
snackBar(String message){
  return SnackBar(
    elevation: 25,
    backgroundColor: AppColors.UI_White,
    duration: Duration(milliseconds: 1000),
    shape: RoundedRectangleBorder(side: BorderSide(color: AppColors.Primary,width: 2.0),/*borderRadius: BorderRadius.circular(5.0.sp)*/),
    content: Text(message,style: AppTheme.headline2,),
    action: SnackBarAction(
      label: 'Close'.tr(),
      textColor: AppColors.Primary,
      onPressed: () {
        // Some code to undo the change.
      },
    ),
  );
}
