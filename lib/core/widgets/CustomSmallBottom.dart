import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class CustomSmallButton extends StatelessWidget {
  final String text;

  final Function onClick;

  final Color textColor;

  final Color buttonColor;

  final bool buttonSize;

  CustomSmallButton(
      {@required this.text, @required this.onClick,this.textColor, this.buttonColor, this.buttonSize,});


  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 4.8.h,
      width: buttonSize == null ? 27.8.w : 24.0.w,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(3.0.sp),
        ),
        child: FittedBox(
          fit:BoxFit.fitWidth,
          child: Text(
            text,),
        ),
        onPressed: onClick,
        color:buttonColor,
      ),
    );
  }
}
