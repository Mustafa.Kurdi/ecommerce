import 'package:ecommerce/core/constants/AppColors.dart';
import 'package:ecommerce/core/widgets/CustomSmallBottom.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class TryRequest extends StatelessWidget {
   var onTry;

   TryRequest({this.onTry});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(
            "Error".tr(),
          ),
          Center(
            child: Container(
              child: CustomSmallButton(
                text: "try".tr(),
                onClick: (){
                  onTry();
                },
                textColor: AppColors.UI_White,
                buttonColor: AppColors.Primary,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
