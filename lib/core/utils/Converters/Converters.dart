import 'package:intl/intl.dart';
import 'package:easy_localization/easy_localization.dart';
class MyConverter{

  static bool toBoolean(String str, [bool strict = false]) {
    str =str.toLowerCase();
    if (strict == true) {
      return str == '1' || str == 'true';
    }
    return str != '0' && str != 'false' && str != '';
  }

  static String timeAgo(DateTime d) {
    Duration diff = DateTime.now().difference(d);
    if (diff.inDays > 365)
      return "${(diff.inDays / 365).floor()} ${(diff.inDays / 365).floor() == 1 ? "year".tr() : "years".tr()} ${'ago'.tr()}";
    if (diff.inDays > 30)
      return "${(diff.inDays / 30).floor()} ${(diff.inDays / 30).floor() == 1 ? "month".tr() : "months".tr()} ${'ago'.tr()}";
    if (diff.inDays > 7)
      return "${(diff.inDays / 7).floor()} ${(diff.inDays / 7).floor() == 1 ? "week".tr() : "weeks".tr()} ${'ago'.tr()}";
    if (diff.inDays > 0)
      return "${diff.inDays} ${diff.inDays == 1 ? "day".tr() : "days".tr()} ${'ago'.tr()}";
    if (diff.inHours > 0)
      return "${diff.inHours} ${diff.inHours == 1 ? "hour".tr() : "hours".tr()} ${'ago'.tr()}";
    if (diff.inMinutes > 0)
      return "${diff.inMinutes} ${diff.inMinutes == 1 ? "minute".tr() : "minutes".tr()} ${'ago'.tr()}";
    return "just now".tr();
  }

  static String cut3Char(String text){
    String cut = text.substring(3);
    return cut;
  }
}