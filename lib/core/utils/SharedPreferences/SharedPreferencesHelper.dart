import 'package:ecommerce/core/utils/SharedPreferences/SharedPreferencesProvider.dart';

class AppSharedPreferences {

  //todo move these to constants
  static const KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";
  static const KEY_LANG = "PREF_KEY_LANG";
  static const KEY_INTRO = "PREF_KEY_INTRO";

  static bool initialized;
  static SharedPreferencesProvider _pref;
  static init() async {
    _pref = await SharedPreferencesProvider.getInstance();
  }

  static clear() {
    _pref.clear();
  }

  static clearForLogOut() {
    removeAccessToken();
    removeAccessIntro();
    removeAccessLang();
  }

  //accessToken
  static String get accessToken => _pref.read(KEY_ACCESS_TOKEN);
  static set accessToken(String accessToken) => _pref.save(KEY_ACCESS_TOKEN, accessToken);
  static bool get hasAccessToken => _pref.contains(KEY_ACCESS_TOKEN);
  static removeAccessToken() => _pref.remove(KEY_ACCESS_TOKEN);


  //language
  static String get lang => _pref.read(KEY_LANG);
  static set lang(String lang) => _pref.save(KEY_LANG, lang);
  static bool get hasAccessLang => _pref.contains(KEY_LANG);
  static removeAccessLang() => _pref.remove(KEY_LANG);


  //intro
  static bool get hasAccessIntro => _pref.contains(KEY_INTRO);
  static bool get intro => _pref.read(KEY_INTRO);
  static set intro(bool intro) => _pref.save(KEY_INTRO, intro);
  static removeAccessIntro() => _pref.remove(KEY_INTRO);

}
